import React from 'react';
import {View, Text} from 'react-native';
import Intro from './components/Intro';
import Login from './components/Login';
import Crud from './components/Crud';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import { Provider } from 'react-redux';
import store from './components/src/store/store';


const Stack = createNativeStackNavigator();

const App = (props) => {
  return (
    <Provider store={store}>
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Intro"
          component={Intro}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Login"
          component={Login}
          options={{headerShown: false}}
        />

        
          <Stack.Screen
            name="Crud"
            component={Crud}
            options={{headerShown: false}}
          />
       
      </Stack.Navigator>
    </NavigationContainer>
    </Provider>
  );
};

export default App;
