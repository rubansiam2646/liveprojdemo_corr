import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  TouchableOpacity,
} from 'react-native';
import {connect} from 'react-redux';
import * as Actiontype from './src/store/Actiontype';

const Crud = (props)  => {
  const [name, setname] = useState('');

  const hInput = () => {
    props.addUser(name);
  };
  const DInput = () => {
    props.addUser('');
  };
  

  return (
    <View style={styles.container}>
      <Text style={styles.name}>Enter Name</Text>
      <TextInput style={styles.input} onChangeText={text => setname(text)} />
      <View style={styles.rowbtns}>
        <TouchableOpacity style={styles.btn} onPress={() =>this. hInput()}>
          <Text style={styles.btt}>Add</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.btn1} onPress={DInput}>
          <Text style={styles.btt}>Delete</Text>
        </TouchableOpacity>
      </View>

      <TextInput style={styles.input}>{props.user.username}</TextInput>
      <TouchableOpacity style={styles.btn2} onPress={() => {props.navigation.navigate('Intro')}} >
          <Text style={styles.btt1}>Go to home</Text>
        </TouchableOpacity>


    </View>
  );
};

const mapStateToProps = state => ({user: state.user});

const mapDispatchToprops = dispatch => ({
  addUser: username =>
    dispatch({
      type: Actiontype.ADD_USER,
      payload: {
        username,
      },
    }),
});

const connectComponent = connect(mapStateToProps, mapDispatchToprops);
export default connectComponent(Crud);

// export default Crud;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop:'50%',
  },
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
  },
  btn: {
    backgroundColor: 'blue',
    borderWidth: 1,
    width: 100,
    marginLeft: '40%',
  },
  btt: {
    fontSize: 20,
    color: '#fff',
    textAlign: 'center',
  },
  name: {
    textAlign: 'center',
    color: 'black',
    fontSize: 20,
  },
  output: {
    textAlign: 'center',
    color: 'black',
    fontSize: 20,
    marginTop: 30,
  },

  btn1: {
    backgroundColor: 'blue',
    borderWidth: 1,
    width: 100,
    marginLeft: '40%',
    marginTop: 10,
  },
  
  btn2: {
    backgroundColor: 'blue',
    borderWidth: 1,
    height:40,
    width: 150,
    marginLeft: '30%',
    marginTop: 10,
  },
  btt1: {
    fontSize: 20,
    color: '#fff',
    textAlign: 'center',
  },
});
