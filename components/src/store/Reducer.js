import * as Actiontype from '../store/Actiontype';

const initialState ={
    user:{
        username:'',
    },
};
export const reducer =(state=initialState,action)=>{
    switch (action.type) {
        case Actiontype.ADD_USER:
            return{
               ...state,
               user:{ ...state.user, username: action.payload.username},
            };
        case Actiontype.DELETE_USER:
            return{
            ...state,
            user:{...state.user,username:action.payload.username},

        };
       
    
        default:
            return state;
    }
};
 