import React,{useState} from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';

const Intro = ({navigation}) => {


  return (
    <View>
      <View style={styles.container}>
        <Image style={styles.pic1} source={require('./Images/pic1.png')} />
        <Text style={styles.text1}>Learn on the Go</Text>
        <Text style={styles.text2}>
          Master your skills with fun and learn very fundamentals
        </Text>
      </View>
      <View style={styles.btns}>
      <TouchableOpacity
        style={styles.button1}
        onPress={() => navigation.navigate('Login')}
       
      >
        <Text style={styles.btntxt1}>Login</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.button2}
        onPress={() => navigation.navigate('Crud')}
        
       
      >
        <Text style={styles.btntxt2}>Signup</Text>
      </TouchableOpacity>
      </View>
     
    </View>
  );
};

export default Intro;

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 50,
  },
  pic1: {
    height: 300,
    width: 300,
  },
  cont: {
    flex: 1,
  },
  text1: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'black',
    marginTop: 20,
  },
  textcont: {
    justifyContent: 'center',
    alignContent: 'center',
  },
  text2: {
    fontSize: 20,
    textAlign: 'center',
    justifyContent:'center',
    alignItems:'center',
  },
  btns:
  {
      flexDirection:'row',
      justifyContent:'center',
      marginTop:80,
  },
  button1:{
     backgroundColor:'#0c55ec',
     borderColor: '#0c55ec',
     borderWidth: 1,
     borderRadius: 10,   
     height:50,
     width:150,
     justifyContent:'center',

     alignItems:'center',
     marginRight:30,
  },
  button2:{
    backgroundColor:'#fff',
    borderColor: '#0c55ec',
    borderWidth: 1,
    borderRadius: 10,   
    height:50,
    width:150,
    justifyContent:'center',

    alignItems:'center',
 },
 btntxt1:{
     fontSize:20,
     color:'#fff'
 },
 btntxt2:{
    fontSize:20,
    color:'#0c55ec'
},
});
