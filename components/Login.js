import React from 'react'
import { StyleSheet, Text, View,
    TextInput,
    Platform,
    TouchableOpacity,
    Image,
    Alert,
     } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';
import Checkbox from '@react-native-community/checkbox';

const Login = ({navigation}) => {
    const [name, setname] = React.useState('')
    const [pass, setpass] = React.useState('')

    const onSubmit = () => {
        if(name === '' ){
          Alert.alert('please Fill mail')
        }else if(pass === ''){
          Alert.alert('Please fill password')
        }else {
          Alert.alert('login sucess')
        }
       
       
         
         
          
          
        
       
    
      };
     
    return (
        <View>
            <Icon
          name="arrow-left"
          size={30}
          color="#4F8EF7"
          style={{marginLeft: 20, marginTop: 20}}
          onPress={() => navigation.navigate('Intro')}
        />

        <View style={styles.welcome}>
          <Text style={styles.t_1}>Create</Text>
          <Text style={styles.t_2}>Account</Text>
        </View>
        <View style={styles.forms}>
            <Text style={styles.it1}> your mail</Text>
          <TextInput
            style={styles.input_1}
           
            onChangeText={(text) => setname(text)}
          />
            <Text style={styles.it2}> Password</Text>
          <TextInput
            style={styles.input_2}
           
            underlineColorAndroid="transparent"
            secureTextEntry={true}
            onChangeText={(text) => setpass(text)}
          />
          <View style={styles.cbtn}>
          <Checkbox style={styles.checkbox}/>
          <TouchableOpacity style={styles.agree}
          > 
          <Text style={styles.term}>I agree with terms & condition and privacy policy</Text>
          </TouchableOpacity>
          </View>
           
        </View>
        
        <View style={styles.c_3}>
        
          <TouchableOpacity
            style={styles.button}
            onPress={() => onSubmit()}>
            <Text style={styles.b1}>log in</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.google}>
              <Icon name="google" size={20} style={{color:'#0a58ed'}}/>
              <Text style={styles.googfont}>sign up with google</Text>
          </TouchableOpacity>
          
          <Text
            style={{
              textAlign: 'center',
              marginTop: 10,
              fontSize: 15,
              color: '#200561',
            }}>
            Forgot password?
          </Text>
        </View>
       
      
        </View>
    )
}

export default Login

const styles = StyleSheet.create({
    welcome: {
        marginTop: 60,
        marginLeft: 40,
        fontFamily:"Roboto-MediumItalic",
        
      },
      t_1: {
        fontSize: 30,
        fontWeight: 'bold',
        letterSpacing: 1,
        color: '#18181a',
      },
      t_2: {
        fontSize: 30,
        letterSpacing: 1,
        color: '#18181a',
        fontWeight: 'bold',
      },
      it1:
      {
          fontSize:20,
    
          color:'black',
      },
      it2:
      {
          fontSize:20,
          marginTop:10,
         
          color:'black',
      },
      forms: {
        justifyContent: 'center',
        width: '80%',
        marginTop: 20,
        marginLeft: 40,
      },
      cbtn:{
          flexDirection:'row',
          justifyContent:'space-between',
          

          
      },
      agree:{
          fontSize:15,
          marginLeft:40,
      },
      checkbox:{
          height:30,
          width:30,
          marginTop:20,
      },
      input_1: {
        fontSize: 20,
        borderWidth:1,
        borderColor:'#f0f5fe',
        backgroundColor:'#f0f5fe',
        borderRadius:5,
        shadowColor: "#000",
shadowOffset: {
	width: 0,
	height: 3,
},
shadowOpacity: 0.29,
shadowRadius: 4.65,

elevation: 7,
    
      },
      term:{
          marginTop:25,
          color:'#2e71ee',
          fontSize:15,
      },
      input_2: {
        fontSize: 20,
        borderWidth: 1,
        marginTop: 10,
        backgroundColor:'#f0f5fe',
        borderRadius:5,
        borderColor:'#f0f5fe',
        shadowColor: "#000",
shadowOffset: {
	width: 0,
	height: 3,
},
shadowOpacity: 0.29,
shadowRadius: 4.65,

elevation: 7,
        
      },
      b1: {
        fontSize: 25,
        backgroundColor: 'blue',
        width: '60%',
        textAlign: 'center',
        marginLeft: 70,
        borderWidth: 1,
        borderRadius: 5,
        padding: 5,
        color: 'white',
        fontWeight: '600',
        letterSpacing: 1,
      },
      c_3: {
        marginTop: 70,
      },
      media: {
        textAlign: 'center',
        marginTop: 10,
        color: '#200561',
        fontSize: 20,
      },
     google:{
         justifyContent:'center',
         flexDirection:'row',
         alignItems:'center',
         fontSize: 25,
         backgroundColor: 'white',
         width: '60%',
         textAlign: 'center',
         marginLeft: 70,
         borderWidth: 1,
         borderRadius: 5,
         padding: 5,
         color: '#0a58ed',
         fontWeight: '600',
         letterSpacing: 1,
         marginTop:10,
         height:45,
     },
    googfont:{
    color:'#0a58ed',
    marginLeft:10,
    }
})
